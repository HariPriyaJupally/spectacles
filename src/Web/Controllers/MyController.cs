using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class MyController : Controller
    {
        public IActionResult Fullframe()
        {
            return Content("This is a store where you get Fullframe.");
        }

        public IActionResult Halfframe()
        {
            return Json(new {name = "Halfframe", description = "This is a store where you get Halfframe." });
        }

        public IActionResult Frameless()
        {
            return Content("<html> <body> <h1> This is a store where you get Frameless </body> </html>", "text/html");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
