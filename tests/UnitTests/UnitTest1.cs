using System;
using Xunit;

namespace UnitTests
{
    public class UnitTest1
    {
       

        [Fact]
        public void NegativeIntegersAdd()
        {
            var expected = -7;
            var a = -5;
            var b = -2;
            var actual = Add(a, b);
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(3,7)]
        [InlineData(5,8)]
        [InlineData(9,2)]
       

        int Add(int x, int y)
        {
            return x + y;
        }

        [Theory]
        [InlineData(3)]
        [InlineData(5)]
        [InlineData(9)]

        bool IsEven(int value)
        {
            return value % 2 == 0;
        }
        }
    
}
